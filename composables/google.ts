export const googleLogin = () => {
  if (process.client) {
    const { GOOGLE_CLIENT_ID } = useRuntimeConfig()
    window.location.replace(
        `https://accounts.google.com/o/oauth2/v2/auth?client_id=${GOOGLE_CLIENT_ID}&response_type=code&redirect_uri=http://localhost:3000/auth/google/callback&scope=openid%20email%20profile`
    )
  }
}